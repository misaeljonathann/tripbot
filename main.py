from flask import Flask, request, abort

from linebot import (
    LineBotApi, WebhookHandler
)
from linebot.exceptions import (
    InvalidSignatureError
)
from linebot.models import (
    MessageEvent, TextMessage, TextSendMessage,
)

app = Flask(__name__)

line_bot_api = LineBotApi('EJ4lCyqlhNMQT5snrRsGMsDE/E4csuWEGTw9A89RKwRAhgbT2Q9gAZ12Kwpe6jQBuikcIzbKVQAdlYGgHf7W3J1hy3d30MVUxqjty3em0z9WzC5kzApR/0rq8C38gnRiOpKH4xVNPBumR+xgQDMOuAdB04t89/1O/w1cDnyilFU=')
handler = WebhookHandler('271124b838488efd97156895d2d847be')


@app.route("/callback", methods=['POST'])
def callback():
    # get X-Line-Signature header value
    signature = request.headers['X-Line-Signature']

    # get request body as text
    body = request.get_data(as_text=True)
    app.logger.info("Request body: " + body)

    # handle webhook body
    try:
        handler.handle(body, signature)
    except InvalidSignatureError:
        abort(400)

    return 'OK'


@handler.add(MessageEvent, message=TextMessage)
def handle_message(event):
    line_bot_api.reply_message(
        event.reply_token,
        TextSendMessage(text=event.message.text))


if __name__ == "__main__":
    app.run()
